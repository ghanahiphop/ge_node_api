"use strict";

describe("test search controller", () => {
	var controller = require("../../src/controllers/search");
	var controller = new controller();
	var helper = require("../helper");

	controller.setReqRes({}, helper.getRes())

	it("test invalud json", () => {
		spyOn(controller.res, "end");
		controller.req.rawBody = "test";
		controller.index();
		expect(controller.res.end).toHaveBeenCalledWith("invalid JSON");
	});


	it("test user have pass valid json", () => {
		spyOn(controller.youtube, "search");
		controller.req.rawBody = '{"q":"test"}';
		controller.index();
		expect(controller.youtube.search).toHaveBeenCalledWith(JSON.parse(controller.req.rawBody), jasmine.any(Function));
	});
});


