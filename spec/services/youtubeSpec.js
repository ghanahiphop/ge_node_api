describe("youtube", () => {
	
	let youtubeService = require("../../src/services/youtubeSearch");
	
	it("test search function that does not have the key of 'q'", () => {
		var obj = {},
			cb = jasmine.createSpy('cb');

		youtubeService.search(obj, cb);

		expect(cb).toHaveBeenCalledWith({
			error: true,
			message: "The query string is missing!"
		});
	});


	it("when obj have q search function should be called", () => {
		var obj = {
				q: "test",
				action: "nextPageToken",
				actionValue: "CAIQAA"
			},
			cb = jasmine.createSpy("cb"),
			youtube = youtubeService.getYouTube();

		spyOn(youtube, "search");
		spyOn(youtubeService, "setYouTubeParam");
		
		youtubeService.search(obj, cb);
		
		expect(youtube.search).toHaveBeenCalled();
		expect(youtubeService.setYouTubeParam).toHaveBeenCalledWith("nextPageToken", "CAIQAA");
	});
});