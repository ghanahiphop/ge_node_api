"use strict";

import database from "../libs/database";
import {dbConfig} from "../config/dbConfig";

export default class model {
	constructor() {
		switch(dbConfig.default) {
			case "mongodb":
				this.db = require("../modules/mongodbClient").db;
			case "mysql":
				this.db = new database(dbConfig.mysql.HOST, dbConfig.mysql.USER, dbConfig.mysql.PASSWORD, dbConfig.mysql.DATABASE);
				break;		
			default:
				throw "unsupport database!"
				break;
		}
	}
}


