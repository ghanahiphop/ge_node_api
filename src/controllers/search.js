"use strict";

import controller from "../core/controller";

module.exports = class search extends controller{
	constructor() {
		super();
		this.youtube = require("../services/youtubeSearch.js");
	}

	index() {
		let _self = this;
		
		try{
			var obj = JSON.parse(this.req.rawBody);
		}catch(e){
			this.res.end("invalid JSON");
			return ;
		}
				
		this.youtube.search(obj, (result) => {
			_self.res.send(result);
		});
	}

	relatedVideos(videoId) {
		this.youtube.relatedVideos(videoId, (result) => {
			this.res.send(result);
		});
	}
}