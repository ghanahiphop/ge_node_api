var YouTube = require('youtube-node');

var youTube = new YouTube();

youTube.setKey('AIzaSyAVZa09kKtrcM2GdGeP3P3xhc3ebcvc_4o');

module.exports = {
	config: {
		maxResult: 20
	},

	getYouTube: () => {
		return youTube;
	},	
	
	getError: (msg) => {
		return {
			error: true,
			message: msg
		}
	},

	setYouTubeParam: (action, val) => {
		switch(action) {
			case "nextPageToken":
			case "prevPageToken":
				action = "pageToken";
				break;
		}

		youTube.addParam(action, val);
	},

	apiResponse: function(error, result, cb) {
		if (error) {
			cb(this.getError("api error!"));
		}else {
		    cb(result);
		}
	},

	search: function(obj, cb) {
		let self = this;
		
		if (!obj.q) {
			cb(this.getError("The query string is missing!"));
			return ;
		}
		
		if (obj.action) {
			this.setYouTubeParam(obj.action, obj.actionValue);
		}

		youTube.search(obj.q, this.config.maxResult, (error, result) =>{
			this.apiResponse(error, result, cb);
		});
	},

	relatedVideos: function(videoId, cb) {
		youTube.related(videoId, this.config.maxResult, (error, result) => {
			this.apiResponse(error, result, cb);
		});
	}

}