"use strict";

export default class route {
	constructor(app) {
		this.controllers = {};
		this.app = app;
		this.setRoute();
	}

	response(obj, req, res, params = []) {
		if( typeof this.controllers[ obj.controller ] == 'undefined' ) {
			var controller = require("../controllers/" + obj.controller);	
			this.controllers[ obj.controller ] = new controller();
		}

		this.controllers[obj.controller].setReqRes(req, res);
		this.controllers[obj.controller][obj.action](...params);
	}

	setRoute() {
		let that = this;
		
		this.app.get("/",  (req, res) => {
			that.response({
				controller: "main",
				action: "index"
			}, req, res);
		});

		this.app.post("/", (req, res) => {
			res.end(JSON.stringify(req.body));
		});


		this.app.all("/authCallback/?code=*", (req,res) => {
			console.info("authCallback has been called");
		});


		this.app.post("/search", (req, res) => {
			that.response({
				controller: "search",
				action: "index"
			}, req, res);
		});

		this.app.get("/search/:videoId", (req, res) => {
			that.response({
				controller: "search",
				action: "relatedVideos"
			}, req, res, [req.params.videoId]);
		});
	}
}