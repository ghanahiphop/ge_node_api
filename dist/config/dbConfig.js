"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});
var dbConfig = {
	"default": "mongodb",

	"mongodb": {
		DATABASE: "media"
	},

	"mysql": {
		HOST: "172.28.128.3",
		USER: "root",
		PASSWORD: "roowpw",
		DATABASE: "eshop"
	}
};

exports.dbConfig = dbConfig;