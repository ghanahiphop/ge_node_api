"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.default = expressSetting;

var _clientSessions = require("client-sessions");

var _clientSessions2 = _interopRequireDefault(_clientSessions);

var _bodyParser = require("body-parser");

var _bodyParser2 = _interopRequireDefault(_bodyParser);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function expressSetting(app, express) {
	app.use((0, _clientSessions2.default)({
		cookieName: 'session',
		secret: 'eg[isfd-8yF9-7w2315df{}+Ijsli;;to8',
		duration: 30 * 60 * 1000,
		activeDuration: 5 * 60 * 1000,
		httpOnly: true,
		ephemeral: true
	}));

	app.use(_bodyParser2.default.json());

	app.use(function (req, res, next) {
		var contentType = req.headers['content-type'] || '',
		    mime = contentType.split(';')[0];

		if (mime != 'text/plain') {
			return next();
		}

		var data = '';
		req.setEncoding('utf8');

		req.on('data', function (chunk) {
			data += chunk;
		});

		req.on('end', function () {
			req.rawBody = data;
			next();
		});
	});

	app.use(_bodyParser2.default.urlencoded({ // to support URL-encoded bodies
		extended: true
	}));

	app.use(_bodyParser2.default.raw());

	// app.use(express.static('../public'));
}