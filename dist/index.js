"use strict";

var _express = require("express");

var _express2 = _interopRequireDefault(_express);

var _expressConfig = require("./config/expressConfig.js");

var _expressConfig2 = _interopRequireDefault(_expressConfig);

var _route = require("./routes/route.js");

var _route2 = _interopRequireDefault(_route);

var _dbConfig = require("./config/dbConfig");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
* should check the default database
**/

if (_dbConfig.dbConfig.default === "mongodb") {
	require("./modules/mongodbClient");
}

var app = (0, _express2.default)();

(0, _expressConfig2.default)(app, _express2.default);

app.use(_express2.default.static(__dirname + '/public'));

new _route2.default(app);

app.listen(8080);

console.log("server running at http://127.0.0.1:3000");

// var youtube = require("./services/youtubeSearch")

// youtube.search();