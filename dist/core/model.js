"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _database = require("../libs/database");

var _database2 = _interopRequireDefault(_database);

var _dbConfig = require("../config/dbConfig");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var model = function model() {
	_classCallCheck(this, model);

	switch (_dbConfig.dbConfig.default) {
		case "mongodb":
			this.db = require("../modules/mongodbClient").db;
		case "mysql":
			this.db = new _database2.default(_dbConfig.dbConfig.mysql.HOST, _dbConfig.dbConfig.mysql.USER, _dbConfig.dbConfig.mysql.PASSWORD, _dbConfig.dbConfig.mysql.DATABASE);
			break;
		default:
			throw "unsupport database!";
			break;
	}
};

exports.default = model;