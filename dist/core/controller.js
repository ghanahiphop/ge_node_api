"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var controller = function () {
	function controller() {
		_classCallCheck(this, controller);

		this.models = {};
	}

	_createClass(controller, [{
		key: "setReqRes",
		value: function setReqRes(req, res) {
			this.req = req;
			this.res = res;
		}
	}, {
		key: "loadModel",
		value: function loadModel(modelName) {
			var model = require("../models/" + modelName);
			this.models[modelName] = new model();
		}
	}]);

	return controller;
}();

exports.default = controller;