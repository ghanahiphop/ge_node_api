"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _controller2 = require("../core/controller");

var _controller3 = _interopRequireDefault(_controller2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

module.exports = function (_controller) {
	_inherits(search, _controller);

	function search() {
		_classCallCheck(this, search);

		var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(search).call(this));

		_this.youtube = require("../services/youtubeSearch.js");
		return _this;
	}

	_createClass(search, [{
		key: "index",
		value: function index() {
			var _self = this;

			try {
				var obj = JSON.parse(this.req.rawBody);
			} catch (e) {
				this.res.end("invalid JSON");
				return;
			}

			this.youtube.search(obj, function (result) {
				_self.res.send(result);
			});
		}
	}, {
		key: "relatedVideos",
		value: function relatedVideos(videoId) {
			var _this2 = this;

			this.youtube.relatedVideos(videoId, function (result) {
				_this2.res.send(result);
			});
		}
	}]);

	return search;
}(_controller3.default);