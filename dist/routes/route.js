"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var route = function () {
	function route(app) {
		_classCallCheck(this, route);

		this.controllers = {};
		this.app = app;
		this.setRoute();
	}

	_createClass(route, [{
		key: "response",
		value: function response(obj, req, res) {
			var _controllers$obj$cont;

			var params = arguments.length <= 3 || arguments[3] === undefined ? [] : arguments[3];

			if (typeof this.controllers[obj.controller] == 'undefined') {
				var controller = require("../controllers/" + obj.controller);
				this.controllers[obj.controller] = new controller();
			}

			this.controllers[obj.controller].setReqRes(req, res);
			(_controllers$obj$cont = this.controllers[obj.controller])[obj.action].apply(_controllers$obj$cont, _toConsumableArray(params));
		}
	}, {
		key: "setRoute",
		value: function setRoute() {
			var that = this;

			this.app.get("/", function (req, res) {
				that.response({
					controller: "main",
					action: "index"
				}, req, res);
			});

			this.app.post("/", function (req, res) {
				res.end(JSON.stringify(req.body));
			});

			this.app.all("/authCallback/?code=*", function (req, res) {
				console.info("authCallback has been called");
			});

			this.app.post("/search", function (req, res) {
				that.response({
					controller: "search",
					action: "index"
				}, req, res);
			});

			this.app.get("/search/:videoId", function (req, res) {
				that.response({
					controller: "search",
					action: "relatedVideos"
				}, req, res, [req.params.videoId]);
			});
		}
	}]);

	return route;
}();

exports.default = route;