'use strict';

var YouTube = require('youtube-node');

var youTube = new YouTube();

youTube.setKey('AIzaSyAVZa09kKtrcM2GdGeP3P3xhc3ebcvc_4o');

module.exports = {
	config: {
		maxResult: 20
	},

	getYouTube: function getYouTube() {
		return youTube;
	},

	getError: function getError(msg) {
		return {
			error: true,
			message: msg
		};
	},

	setYouTubeParam: function setYouTubeParam(action, val) {
		switch (action) {
			case "nextPageToken":
			case "prevPageToken":
				action = "pageToken";
				break;
		}

		youTube.addParam(action, val);
	},

	apiResponse: function apiResponse(error, result, cb) {
		if (error) {
			cb(this.getError("api error!"));
		} else {
			cb(result);
		}
	},

	search: function search(obj, cb) {
		var _this = this;

		var self = this;

		if (!obj.q) {
			cb(this.getError("The query string is missing!"));
			return;
		}

		if (obj.action) {
			this.setYouTubeParam(obj.action, obj.actionValue);
		}

		youTube.search(obj.q, this.config.maxResult, function (error, result) {
			_this.apiResponse(error, result, cb);
		});
	},

	relatedVideos: function relatedVideos(videoId, cb) {
		var _this2 = this;

		youTube.related(videoId, this.config.maxResult, function (error, result) {
			_this2.apiResponse(error, result, cb);
		});
	}

};