"use strict";

var _dbConfig = require("../config/dbConfig");

// Retrieve
var MongoClient = require('mongodb').MongoClient;
// Connect to the db
MongoClient.connect("mongodb://localhost:27017/" + _dbConfig.dbConfig.mongodb.DATABASE, { native_parser: true }, function (err, db) {
    if (!err) {
        console.log("db connect successfully!");
        MongoClient.db = db;
    } else {
        console.log("connect to db fail, the error message is:", err);
    }
});

module.exports = MongoClient;