"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _mysql = require("mysql");

var _mysql2 = _interopRequireDefault(_mysql);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// import Q from "q";

var database = function () {
	function database(HOST, USER, PASSWORD, DATABASE) {
		_classCallCheck(this, database);

		this.connection = _mysql2.default.createConnection({
			host: HOST,
			user: USER,
			password: PASSWORD,
			database: DATABASE
		});

		this.connection.connect();
	}

	_createClass(database, [{
		key: "query",
		value: function query(queryStr) {
			var _self = this;

			return new Promise(function (resolve, reject) {
				_self.connection.query(queryStr, function (err, rows, fields) {
					if (!err) {
						resolve(rows);
					} else {
						reject(err);
					}
				});
			});
		}
	}]);

	return database;
}();

exports.default = database;